FROM python:3.8

WORKDIR /src

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY src /src
COPY docker /docker
COPY Pipfile /src/Pipfile
COPY Pipfile.lock /src/Pipfile.lock

RUN pip3 install pipenv
RUN pipenv install --deploy --system

CMD ["/docker/start.sh"]
