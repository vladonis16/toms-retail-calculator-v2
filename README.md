# Tom's retail calculator

## Task details:
Calculator must define order's total price based on parameters:

* Item's price per piece;
* Quantity of items;
* Discount percent, depending on total price of items in order;
* State tax percent.

State tax and discount percents are predefined and located in `state_tax_dict.csv` and `discount_dict.csv` accordingly.

## Run and configure with docker-compose:

Build and run docker containers:

```
docker-compose up
```

## API docs:

Swagger: http://127.0.0.1:8000/swagger/

## Important note:

Data needed by the task returns in `POST /order/` request

Each order has price information:

* Net total: total sum of all order's items;
* Discount total: net total with applied discount;
* `Total`: Discount total with applied state tax - the sum, needed to be calculated by the task.


## Todo's and assumptions

It is assumpted that if no tax presented in database, then 0% tax is applied

Improvement can be reached by adding admin side, unittest coverage
