from enum import unique
from django.db import models
from localflavor.us.models import USStateField


class Tax(models.Model):
    state = USStateField(unique=True)
    tax_percent = models.DecimalField(max_digits=5, decimal_places=2)
