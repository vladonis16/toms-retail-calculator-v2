# Generated by Django 3.2 on 2021-09-29 14:52

from django.db import migrations
import localflavor.us.models


class Migration(migrations.Migration):

    dependencies = [("tax", "0002_manually_create_taxes")]

    operations = [
        migrations.AlterField(
            model_name="tax",
            name="state",
            field=localflavor.us.models.USStateField(max_length=2, unique=True),
        )
    ]
