from enum import unique
from django.db import models


class Discount(models.Model):
    price_gte = models.DecimalField(max_digits=15, decimal_places=2, unique=True)
    discount_percent = models.DecimalField(max_digits=5, decimal_places=2)
