from rest_framework import generics, status, response, filters
from . import models, serializers
from rest_framework.permissions import AllowAny
from utils.paginations import OrderListPagination


class OrderListView(generics.ListAPIView):
    serializer_class = serializers.OrderSerializer
    permission_classes = [AllowAny]
    filter_backends = [filters.OrderingFilter]
    ordering_fields = "__all__"
    ordering = ["-date_created"]
    queryset = models.Order.objects.all()
    pagination_class = OrderListPagination

    def get(self, request, *args, **kwargs):
        """
        Get list of all orders
        """
        return super().get(request, *args, **kwargs)


class OrderView(generics.GenericAPIView):
    serializer_class = serializers.OrderSerializer
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        """
        Create order
        """
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data = serializer.save(request)
        serializer = self.get_serializer(data)
        return response.Response(serializer.data, status=status.HTTP_201_CREATED)


class OrderDetailsView(generics.GenericAPIView):
    serializer_class = serializers.OrderSerializer
    permission_classes = [AllowAny]

    def get(self, request, id):
        """
        Get order by id
        """
        order = models.Order.objects.filter(pk=id).first()

        if not order:
            return response.Response(
                {"error": "Order not found"}, status=status.HTTP_404_NOT_FOUND
            )
        serializer = self.get_serializer(order)
        return response.Response(serializer.data)
