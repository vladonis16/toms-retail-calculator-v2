from django.db import models
from django.core.validators import MinValueValidator
from localflavor.us.models import USStateField


class Order(models.Model):
    state = USStateField()
    date_created = models.DateTimeField(auto_now_add=True)
    discount_percent = models.DecimalField(max_digits=5, decimal_places=2)
    tax_percent = models.DecimalField(max_digits=5, decimal_places=2)
    net_total = models.DecimalField(max_digits=15, decimal_places=2)
    discount_total = models.DecimalField(max_digits=15, decimal_places=2)
    total = models.DecimalField(max_digits=15, decimal_places=2)


class OrderItem(models.Model):
    name = models.CharField(max_length=100)
    quantity = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    price = models.DecimalField(
        max_digits=15, decimal_places=2, validators=[MinValueValidator(0.01)]
    )
    order = models.ForeignKey(Order, related_name="items", on_delete=models.CASCADE)
