from rest_framework import serializers
from .models import OrderItem, Order
from .calculations import precalculate_order


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ("id", "name", "quantity", "price")
        read_only_fields = ("id",)


class OrderSerializer(serializers.ModelSerializer):
    items = OrderItemSerializer(many=True)

    class Meta:
        model = Order
        fields = (
            "id",
            "state",
            "date_created",
            "discount_percent",
            "tax_percent",
            "net_total",
            "discount_total",
            "total",
            "items",
        )
        read_only_fields = (
            "id",
            "date_created",
            "discount_percent",
            "tax_percent",
            "net_total",
            "discount_total",
            "total",
        )

    def save(self, request):
        items = request.data.pop("items")
        calculated_order_data = precalculate_order(request.data["state"], items)

        order = Order.objects.create(**calculated_order_data)

        for item in items:
            item_instance = OrderItem(order_id=order.id, **item)
            item_instance.save()

        return order
