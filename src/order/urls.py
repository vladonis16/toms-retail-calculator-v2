from django.urls import path
from . import views

urlpatterns = [
    path("", views.OrderView.as_view(), name="order"),
    path("<int:id>", views.OrderDetailsView.as_view(), name="order_details"),
    path("list", views.OrderListView.as_view(), name="order_list"),
]
