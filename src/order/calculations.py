from tax.models import Tax
from decimal import Decimal
from discount.models import Discount


def precalculate_order(state, items):
    net_total = sum(Decimal(i["price"]) * i["quantity"] for i in items)
    discount = (
        Discount.objects.filter(price_gte__lte=net_total).order_by("-price_gte").first()
    )

    discount_total = round(net_total - (net_total * discount.discount_percent / 100), 2)

    tax = Tax.objects.filter(state=state).first()
    if tax:
        tax_percent = tax.tax_percent
    else:
        tax_percent = 0

    total = round(
        discount_total + (Decimal(tax_percent) * discount_total * Decimal(0.01)), 2
    )

    return {
        "state": state,
        "net_total": net_total,
        "discount_percent": discount.discount_percent,
        "discount_total": discount_total,
        "tax_percent": tax_percent,
        "total": total,
    }
